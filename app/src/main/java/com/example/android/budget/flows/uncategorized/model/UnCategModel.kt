package com.example.android.budget.flows.uncategorized.model

data class UnCategModel(val id: Int, val title: String, val date: String, val amount: String) {
}