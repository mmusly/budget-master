package com.example.android.budget.flows.tranlink.model

data class CategoryModel (val id: Int, val name: String, val limit: Double, val current: Double){
}