package com.example.android.budget.flows.tranlink

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.android.budget.R
import com.example.android.budget.databinding.ListItemCategoryBinding
import com.example.android.budget.flows.common.DataBoundListAdapter
import com.example.android.budget.flows.tranlink.model.CategoryModel

class TranLinkAdapter(private val itemClickCallback: ((CategoryModel) -> Unit)?) : DataBoundListAdapter<CategoryModel, ListItemCategoryBinding>(
    diffCallback = object : DiffUtil.ItemCallback<CategoryModel>() {
        override fun areItemsTheSame(oldItem: CategoryModel, newItem: CategoryModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CategoryModel, newItem: CategoryModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ListItemCategoryBinding {
        val binding = DataBindingUtil.inflate<ListItemCategoryBinding>(
            LayoutInflater.from(parent.context), R.layout.list_item_category,
            parent, false
        )

        binding.constraintCategoryItem.setOnClickListener {
            binding.data?.let {
                itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ListItemCategoryBinding, item: CategoryModel) {
        binding.data = item
    }
}