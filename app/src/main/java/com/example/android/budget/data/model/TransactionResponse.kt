package com.example.android.budget.data.model

import com.google.gson.annotations.SerializedName

data class TransactionResponse(

    @field:SerializedName("amount")
    val amount: Double? = null,

    @field:SerializedName("TIMESTAMP")
    val tIMESTAMP: String? = null,

    @field:SerializedName("customerid")
    val customerid: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("curr")
    val curr: String? = null,

    @field:SerializedName("origid")
    val origid: String? = null,

    @field:SerializedName("creditcardnumber")
    val creditcardnumber: String? = null,

    @field:SerializedName("desc")
    val desc: String? = null
)