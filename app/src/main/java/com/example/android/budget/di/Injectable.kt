package com.example.android.budget.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
