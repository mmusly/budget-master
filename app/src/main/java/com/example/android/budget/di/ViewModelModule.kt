package com.example.android.budget.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android.budget.flows.featurex.FeatureXViewModel
import com.example.android.budget.flows.featurey.FeatureYViewModel
import com.example.android.budget.flows.featurez.FeatureZViewModel
import com.example.android.budget.flows.home.HomeViewModel
import com.example.android.budget.flows.home.homefragments.FragmentAViewModel
import com.example.android.budget.flows.tranlink.TranLinkViewModel
import com.example.android.budget.flows.uncategorized.UnCategorizedViewModel
import com.example.android.budget.viewmodel.ViewModelProviderFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Uses dagger multi-binding to provide [ViewModel] instances used in the app.
 *
 * @see <a href="https://dagger.dev/multibindings">Multibindings</a>
 */
@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeatureXViewModel::class)
    abstract fun bindFeatureXViewModel(viewModel: FeatureXViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UnCategorizedViewModel::class)
    abstract fun bindUncategorizedViewModel(viewModel: UnCategorizedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TranLinkViewModel::class)
    abstract fun bindTranLinkViewModel(viewModel: TranLinkViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeatureYViewModel::class)
    abstract fun bindFeatureYViewModel(viewModel: FeatureYViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeatureZViewModel::class)
    abstract fun bindFeatureZViewModel(viewModel: FeatureZViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FragmentAViewModel::class)
    abstract fun bindFragmentAViewModel(viewModel: FragmentAViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}
