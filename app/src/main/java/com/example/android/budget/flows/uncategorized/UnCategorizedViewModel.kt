package com.example.android.budget.flows.uncategorized

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.budget.api.WebServiceApi
import com.example.android.budget.data.model.TransactionResponse
import com.example.android.budget.flows.common.Result
import com.example.android.budget.flows.uncategorized.model.UnCategModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.SimpleDateFormat
import javax.inject.Inject

class UnCategorizedViewModel @Inject constructor(private val api: WebServiceApi): ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)
    val message = ObservableField<Result<ArrayList<TransactionResponse>>>()

    private val _data = MutableLiveData<List<UnCategModel>>()
    val data: LiveData<List<UnCategModel>> = _data


    private val sampleData = mutableListOf<UnCategModel>()

    init {
        this.onSendWebRequest("111")
    }

    fun onSendWebRequest(custid: String) {
        Timber.i("Sending web request with customerID: $custid")
        compositeDisposable.add(
            api.getUncategorizedTransactions(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    message.set(Result.Success(it))
                    generateDataSet(it)
                    _data.value = sampleData.toList()
                }, { error ->
                    Timber.e(error)
                    message.set(Result.Error(error))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun generateDataSet(list: ArrayList<TransactionResponse>) {
        sampleData.clear()
        for (i in 1..list.size-1) {

            val inputDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val outputDate = SimpleDateFormat("dd MMM YYYY")
            val d = inputDate.parse(list[i].tIMESTAMP.toString())

            val dateFormatted = outputDate.format(d)
            sampleData.add(UnCategModel(list[i].id!!, list[i].desc.toString(), dateFormatted, list[i].amount.toString()))
        }
    }

    fun onItemClicked(item: UnCategModel) {
        sampleData.remove(item)
        _data.value = sampleData.toList()

    }
}