package com.example.android.budget.flows.uncategorized

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.example.android.budget.R
import com.example.android.budget.databinding.ListItemUncategorizedBinding
import com.example.android.budget.flows.common.DataBoundListAdapter
import com.example.android.budget.flows.uncategorized.model.UnCategModel


class UnCategorizedAdapter(private val itemClickCallback: ((UnCategModel) -> Unit)?) : DataBoundListAdapter<UnCategModel, ListItemUncategorizedBinding>(
    diffCallback = object : DiffUtil.ItemCallback<UnCategModel>() {
        override fun areItemsTheSame(oldItem: UnCategModel, newItem: UnCategModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: UnCategModel, newItem: UnCategModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ListItemUncategorizedBinding {
        val binding = DataBindingUtil.inflate<ListItemUncategorizedBinding>(
            LayoutInflater.from(parent.context), R.layout.list_item_uncategorized,
            parent, false
        )

        binding.constraintItem.setOnClickListener {
            binding.data?.let {
                itemClickCallback?.invoke(it)
            }
        }
        return binding
    }

    override fun bind(binding: ListItemUncategorizedBinding, item: UnCategModel) {
        binding.data = item
    }
}
