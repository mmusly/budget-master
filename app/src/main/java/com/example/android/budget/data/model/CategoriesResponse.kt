package com.example.android.budget.data.model

import com.google.gson.annotations.SerializedName

data class CategoriesResponse(

	@field:SerializedName("period")
	val period: Int? = null,

	@field:SerializedName("limitamount")
	val limitamount: Double? = null,

	@field:SerializedName("currentamount")
	val currentamount: Double? = null,

	@field:SerializedName("custid")
	val custid: Int? = null,

	@field:SerializedName("categoryname")
	val categoryname: String? = null,

	@field:SerializedName("categoryid")
	val categoryid: Int? = null
)