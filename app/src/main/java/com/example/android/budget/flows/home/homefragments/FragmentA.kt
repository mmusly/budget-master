package com.example.android.budget.flows.home.homefragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.android.budget.R
import com.example.android.budget.databinding.FragmentDemoContentABinding
import com.example.android.budget.di.Injectable
import com.example.android.budget.flows.featurex.FeatureXActivity
import com.example.android.budget.flows.featurey.FeatureYActivity
import com.example.android.budget.flows.featurez.FeatureZActivity
import javax.inject.Inject
import timber.log.Timber

/**
 * Demo fragment for tab content.
 *
 * This fragment is different than [FragmentB] and [FragmentC] in following ways:
 * - This has it's own view model [FragmentAViewModel] which is provided via injected factory.
 * - This loads a custom layout to test other activities.
 *
 * TODO: Move the fragment to it's own feature package.
 */
class FragmentA : Fragment(), Injectable {
    companion object {
        fun createInstance(): FragmentA {
            return FragmentA()
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentDemoContentABinding
    lateinit var viewModel: FragmentAViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = activity?.run {
            ViewModelProviders.of(this, viewModelFactory).get(FragmentAViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        Timber.d("Got injected fragment's own viewmodel instance: %s.", viewModel)

        // Inflate the layout for this fragment using data binding and set the view model
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_demo_content_a, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigationEvents(viewModel)
    }

    /**
     * TODO: This is an example of how LiveData can be used to navigate. Update accordingly.
     */
    private fun observeNavigationEvents(viewModel: FragmentAViewModel) {
        viewModel.featureXEvent.observe(this, Observer {
            Timber.i("Launching feature X activity.")
            startActivity(Intent(activity, FeatureXActivity::class.java))
        })

        viewModel.featureYEvent.observe(this, Observer {
            Timber.i("Launching feature Y activity.")
            startActivity(Intent(activity, FeatureYActivity::class.java))
        })

        viewModel.featureZEvent.observe(this, Observer {
            Timber.i("Launching feature Z activity.")
            startActivity(Intent(activity, FeatureZActivity::class.java))
        })
    }
}
