package com.example.android.budget.flows.tranlink

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.budget.api.WebServiceApi
import com.example.android.budget.data.model.CategoriesResponse
import com.example.android.budget.data.model.UpdateRowResponse
import com.example.android.budget.flows.common.Result
import com.example.android.budget.flows.tranlink.model.CategoryModel
import com.example.android.budget.flows.tranlink.model.UpdateTranCategRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class TranLinkViewModel @Inject constructor(private val api: WebServiceApi): ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    val isOperationInProgress = ObservableField(false)
    val message = ObservableField<Result<ArrayList<CategoriesResponse>>>()
    val updateResponse = ObservableField<Result<UpdateRowResponse>>()

    private val _data = MutableLiveData<List<CategoryModel>>()
    val data: LiveData<List<CategoryModel>> = _data
    private val sampleData = mutableListOf<CategoryModel>()

    init {
        this.onGetCategoriesRequest(111, 202002)
    }

    fun onGetCategoriesRequest(custid: Int, period: Int) {

        compositeDisposable.add(
            api.getCategories(custid, period)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    message.set(Result.Success(it))
                    generateDataSet(it)
                    _data.value = sampleData.toList()
                }, { error ->
                    Timber.e(error)
                    message.set(Result.Error(error))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun generateDataSet(list: ArrayList<CategoriesResponse>) {
        sampleData.clear()
        for (i in 1..list.size-1) {

            sampleData.add(CategoryModel(list[i].categoryid!!, list[i].categoryname!!, list[i].limitamount!!, list[i].currentamount!!))
        }
    }


    fun onItemClicked(item: CategoryModel, tranId: Int, note: String) {
        updateTranCategory(item,tranId, note)
        _data.value = sampleData.toList()
    }


    fun updateTranCategory(categItem: CategoryModel, tranID: Int, note: String) {

        val bodyObj = UpdateTranCategRequest(tranID, categItem.id, categItem.name, note)

        compositeDisposable.add(
            api.updateTranCategory(bodyObj)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isOperationInProgress.set(true) }
                .doOnSuccess { isOperationInProgress.set(false) }
                .doOnError { isOperationInProgress.set(false) }
                .subscribe({
                    updateResponse.set(Result.Success(it))
                    _data.value = sampleData.toList()
                }, { error ->
                    Timber.e(error)
                    updateResponse.set(Result.Error(error))
                })
        )
    }

}