package com.example.android.budget.api

import com.example.android.budget.data.model.CategoriesResponse
import com.example.android.budget.data.model.ResponseMessage
import com.example.android.budget.data.model.TransactionResponse
import com.example.android.budget.data.model.UpdateRowResponse
import com.example.android.budget.flows.tranlink.model.UpdateTranCategRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WebServiceApi {

    @GET("/say/hello")
    fun hello(@Query("name") name: String): Single<ResponseMessage>

    @GET("getuncategorizedtransactions")
    fun getUncategorizedTransactions(
        @Query("custid") custid: String
    ): Single<ArrayList<TransactionResponse>>

    @GET("getcategories")
    fun getCategories(
        @Query("custid") custid: Int,
        @Query("period") period: Int
    ): Single<ArrayList<CategoriesResponse>>

    @POST("updatetrancategory")
    fun updateTranCategory(
        @Body body: UpdateTranCategRequest
    ): Single<UpdateRowResponse>



}
