package com.example.android.budget.flows.featurey.model

/**
 * Sample model class used in list to showcase recycler view.
 */
data class ItemModel(val id: Int, val title: String)
