package com.example.android.budget.flows.tranlink.model

import com.google.gson.annotations.SerializedName


data class UpdateTranCategRequest(

	@field:SerializedName("tranID")
	val tranID: Int? = null,

	@field:SerializedName("categid")
	val categid: Int? = null,

	@field:SerializedName("categName")
	val categName: String? = null,

	@field:SerializedName("tranNote")
	val tranNote: String? = null
)