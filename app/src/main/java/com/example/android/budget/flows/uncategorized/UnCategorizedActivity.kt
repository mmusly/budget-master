package com.example.android.budget.flows.uncategorized
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.budget.R
import com.example.android.budget.databinding.ActivityUnCategorizedBinding
import com.example.android.budget.flows.common.Result
import com.example.android.budget.flows.extensions.onChanged
import com.example.android.budget.flows.tranlink.TranLinkActivity
import dagger.android.AndroidInjection
import timber.log.Timber
import javax.inject.Inject

class UnCategorizedActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: UnCategorizedViewModel
    private lateinit var binding: ActivityUnCategorizedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_un_categorized)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UnCategorizedViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.message.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.i("Result.Success retrieve data")
                    setupIdeaList(binding.uncategorizedRecycler)
                }
                is Result.Error -> {
                    Timber.i("Result.Error")
                    //setupIdeaList(binding.uncategorizedRecycler)
                    //Toast.makeText(this, result.exception.toString(), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun setupIdeaList(ideaList: RecyclerView) {

        val unCategAdapter = UnCategorizedAdapter { data ->
            Timber.d("Item Clicked: $data")
            viewModel.onItemClicked(data)

            val intent = Intent(this, TranLinkActivity::class.java)
            intent.putExtra("id", data.id)
            intent.putExtra("date", data.date)
            intent.putExtra("desc", data.title)
            intent.putExtra("amount", data.amount)
            startActivityForResult(intent, 3)
        }

        ideaList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@UnCategorizedActivity)
            adapter = unCategAdapter
        }

        viewModel.data.observe(this, Observer { result ->
            unCategAdapter.submitList(result)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}
