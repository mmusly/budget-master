package com.example.android.budget.flows.tranlink

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.budget.R
import com.example.android.budget.databinding.ActivityTranLinkBinding
import com.example.android.budget.flows.common.Result
import com.example.android.budget.flows.extensions.onChanged
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_tran_link.*
import timber.log.Timber
import javax.inject.Inject

class TranLinkActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: TranLinkViewModel
    private lateinit var binding: ActivityTranLinkBinding

    private var dateData = ""
    private var descData = ""
    private var amountData = ""
    private var idData = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tran_link)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(TranLinkViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        idData = intent.getIntExtra("id", 0)
        dateData = intent.getStringExtra("date")
        descData = intent.getStringExtra("desc")
        amountData = intent.getStringExtra("amount")

        tranLinkDesc.text = descData
        tranLinkDate.text = dateData
        tranLinkAmount.text = amountData

        viewModel.message.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.i("Result.Success retrieve data")
                    setupIdeaList(binding.tranLinkRecycler)
                }
                is Result.Error -> {
                    Timber.i("Result.Error")
                }
            }
        }

        viewModel.updateResponse.onChanged { result ->
            when(result) {
                is Result.Success -> {
                    Timber.i("Result.Success retrieve data")

                    val data = Intent()
//                    data.putExtra("streetkey", "streetname")
//                    data.putExtra("citykey", "cityname")
//                    data.putExtra("homekey", "homename")
                    setResult(Activity.RESULT_OK, null)
                    finish()
                }
                is Result.Error -> {
                    Timber.i("Result.Error")
                }
            }
        }
    }

    private fun setupIdeaList(ideaList: RecyclerView) {

        val categoryAdapter = TranLinkAdapter { data ->
            Timber.d("Item Clicked: $data")
            viewModel.onItemClicked(data, idData, editText.text.toString())
        }

        ideaList.apply {
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(this@TranLinkActivity)
            adapter = categoryAdapter
        }

        viewModel.data.observe(this, Observer { result ->
            categoryAdapter.submitList(result)
        })
    }
}
